
Drupal.behaviors.vbo_barcode_select = function(context) {

  var barcode = $('<div id="vbo-barcode-select"><label>' + Drupal.t('Barcode:') + '</label> </div>');
  var input = $('<input type="text" size="20" name="vbo_barcode_select" />');

  input.keydown(function(e) {

    var selectedBarcode = $(this).val();
    var selector = 'table.views-table tbody tr td.' + Drupal.settings.vbo_barcode_select;
    var found = false;

    // Check for matching barcodes on ENTER/RETURN.

    if (e.keyCode == 13) {

      // Ignore blank barcodes.

      if (selectedBarcode != '') {

        $(selector).each(function() {

          var td = $(this);
          var tr = td.parents('tr');

          if (td.text() == selectedBarcode) {

            // Select the VBO checkbox on match.

            tr.find('input.vbo-select').attr('checked', 'checked');
            found = true;

          } // if

        });

        if (!found) {

          vboBarCodeSelectShowAlert('Barcode not found.');

        } // if

      } // if

      return false;

    } // if

  });

  input.appendTo(barcode);

  barcode.insertBefore('.views-node-selector')

} // function

function vboBarCodeSelectShowAlert(message) {

  $('<b class="error"> ' + message + '</b>')
    .insertAfter('#vbo-barcode-select input')
    .fadeOut(1500, function() {
      
      $(this).remove();
      
    });

} // vboBarCodeSelectShowAlert
